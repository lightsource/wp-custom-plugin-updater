<?php

// todo some validation

$fileName = 'download.zip';

header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header("Cache-Control: no-cache, must-revalidate");
header("Expires: 0");
header('Content-Disposition: attachment; filename="' . basename($fileName) . '"');
header('Content-Length: ' . filesize($fileName));
header('Pragma: public');

flush();
readfile($fileName);
exit;
