<?php
/*
Plugin Name: Test
Plugin URI: https://wordpress.org/
Description: Some description
Version: 1.0.0
Author: Maxim Akimov
Author URI: https://wordpress.org/
*/

class Test
{
    private string $pluginSlug;

    public function __construct()
    {
        $this->pluginSlug = plugin_basename(__DIR__);

        add_filter('plugins_api', [$this, 'getUpdateInfo'], 20, 3);
        add_filter('site_transient_update_plugins', [$this, 'getUpdate']);
    }

    public function getUpdateInfo($result, $action, $args)
    {
        if ('plugin_information' !== $action ||
            $this->pluginSlug !== $args->slug) {
            return $result;
        }

        $result = new stdClass();

        $result->name           = 'Name';
        $result->slug           = $this->pluginSlug;
        $result->version        = 'version';
        $result->tested         = 'tested';
        $result->requires       = 'requires';
        $result->author         = 'author';
        $result->author_profile = 'author_profile';
        $result->download_link  = 'download_link';
        $result->trunk          = 'trunk';
        $result->requires_php   = 'requires_php';
        $result->last_updated   = 'last_updated';

        $result->sections = [
            'description'  => 'description',
            'installation' => 'installation',
            'changelog'    => 'changelog'
        ];

        if (! empty($remote->banners)) {
            $result->banners = [
                'low'  => $remote->banners->low,
                'high' => $remote->banners->high
            ];
        }

        return $result;
    }

    public function getUpdate($transient)
    {
        if (empty($transient->checked)) {
            return $transient;
        }

        // todo some checking of updates
        if (false) {
            return $transient;
        }

        $res                               = new stdClass();
        $res->slug                         = $this->pluginSlug;
        $res->plugin                       = plugin_basename(__FILE__);
        $res->new_version                  = '2.0.0';
        $res->tested                       = 'tested';
        $res->package                      = 'http://angama.loc/download.php';
        $transient->response[$res->plugin] = $res;

        return $transient;
    }
}

new Test();
